/* App Parts */
// import './dynamicImport.js'

/* jquery */
import $ from 'jquery'
import ResponsiveMenu from './parts/responsiveMenu.js'

/* AOS */
import AOS from 'aos'
import '../../node_modules/aos/dist/aos.css'

/* Magnific Popup */
import '../../node_modules/magnific-popup/dist/jquery.magnific-popup.js'
import '../../node_modules/magnific-popup/dist/magnific-popup.css'

import '../css/font-awesome.min.css'
import './parts/parallax.js'
import './modules/accordion.js'
import './modules/videos.js'

AOS.init({
  duration: 1500
})

new ResponsiveMenu('#mainNav')
