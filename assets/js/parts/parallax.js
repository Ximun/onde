let layers = document.querySelectorAll('[data-type="parallax"]')
let height = document.body.offsetHeight

let down = document.getElementById('down')
let maelstrom = document.getElementById('maelstrom')
let actus = document.getElementById('actus')

let offset = 200
let position = 'photo'

let $ = require('jquery')

function parallax (layers) {
  let distanceBottom = document.body.offsetHeight - (window.pageYOffset + window.innerHeight)
  if (distanceBottom <= 600) {
    layers.forEach((layer) => {
      let depth = layer.getAttribute('data-depth')
      let movement = (distanceBottom * depth)
      let translate3d = 'translate3d(0, ' + movement + 'px, 0)'
      layer.style.transform = translate3d
    })
  }
  if (distanceBottom === 0) {
    document.querySelector('#groundLight').style.visibility = 'visible'
    document.querySelector('#groundLight').style.opacity = 0.9
    document.querySelector('#light').style.visibility = 'visible'
    document.querySelector('#light').style.opacity = 0.9
  }
}

function arrow () {
  console.log(window.pageYOffset)
  if (window.pageYOffset < offset) {
    down.className = 'white'
    position = 'photo'
  } else if ((window.pageYOffset >= offset) && (window.pageYOffset < (maelstrom.offsetTop + offset))) {
    down.className = 'red'
    position = 'maelstrom'
  } else if ((window.pageYOffset >= (maelstrom.offsetTop + offset))) {
    down.className = 'hidden'
    position = 'actus'
  }
}

parallax(layers)
window.addEventListener('scroll', function () {
  parallax(layers)
  if (window.location.pathname === '/') {
    arrow()
  }
})

if (window.location.pathname === '/') {
  down.addEventListener('click', function (e) {
   if (position === 'photo') {
      e.preventDefault()
      $('body,html').animate({
        scrollTop: maelstrom.offsetTop,
        easing: 'easeInOutQuad'
      }, 2000)
    } else if (position === 'maelstrom') {
      e.preventDefault()
      $('body,html').animate({
        scrollTop: actus.offsetTop,
        easing: 'easeInOutQuad'
      }, 2000)
    }
  })
}
