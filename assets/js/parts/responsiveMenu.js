export default class ResponsiveMenu {
    /**
     * @param (string) selector
     */
    constructor (selector) {
        this.activatedClass = 'responsive-activated'
        this.createSidebar()
        this.submenus = Array.from(document.querySelectorAll('#responsiveNav .submenu'))
        this.submenusLogic()

        this.eventsListener()
    }

    /*
     * Create sidebar by cloning main menu content
     */
    createSidebar () {
        this.content = document.getElementById('mainNav')
        this.menu = document.getElementById('menu')
        this.sidebar = document.createElement('div')
        this.sidebar.setAttribute('id', 'responsiveNav')
        this.overlay = document.getElementById('responsiveOverlay')
        this.button = document.getElementById('responsiveButton')

        this.menu.appendChild(this.sidebar)
        this.sidebar.innerHTML = this.content.innerHTML
    }

    /*
     * Submenu Logic
     */
    submenusLogic () {
        this.submenus.forEach((submenu) => {
            let height = submenu.offsetHeight
            submenu.style.height = '0px'
            submenu.style.transitionDuration = '.5s'
            submenu.parentElement.addEventListener('click', (e) => {
                this.submenus.forEach((e) => { e.style.height = '0px' })
                // e.preventDefault()
                submenu.style.height = height + 'px'
            })
        })
    }

    /*
     * Events listened for ResponsiveMenu logic
     */
    eventsListener () {
        // A faire: Ajouter le glissé smartphone aux events

        /*
         * Open Resp Menu by clicking on button
         */
        this.button.addEventListener('click', (e) => {
            e.preventDefault()
            this.sidebar.classList.add(this.activatedClass)
            this.overlay.classList.add(this.activatedClass)
        })

        /*
         * Close Resp Menu by clicking on overlay
         */
        this.overlay.addEventListener('click', (e) => {
            e.preventDefault()
            this.sidebar.classList.remove(this.activatedClass)
            this.overlay.classList.remove(this.activatedClass)
        })
    }
}
