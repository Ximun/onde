import $ from 'jquery'
import 'magnific-popup'
import 'magnific-popup/dist/magnific-popup.css'

// Display youtube videos gallery using magnific-popup.js
if (document.contains(document.querySelector('.video-element'))) {
  $(function () {
    $('.video-element').each(function () {
      // $(this).append('<img src="https://img.youtube.com/vi/' + $(this).attr('video-id') + '/maxresdefault.jpg"/>')
      var id = $(this).attr('video-id')
      $(this).magnificPopup({
        items: [
          {
            src: 'https://www.youtube.com/watch?v=' + id,
            type: 'iframe'
          }
        ]
      })
    })
  })
}

// Photos galeries
if (document.contains(document.querySelector('.gallerie'))) {
  $(function () {
    $('.gallerie').each(function () {
      $(this).magnificPopup({
        delegate: 'img',
        type: 'image',
        gallery: {
          enabled: true
        }
      })
    })
  })
}
