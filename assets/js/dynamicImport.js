function dynamicImport (selector, file) {
  if (document.contains(document.querySelector(selector))) {
    import('./modules/' + file + '.js').then({})
  }
}

dynamicImport('.accordion', 'accordion')
