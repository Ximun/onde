let offset = 600,
    offset_opacity = 1000,
    scroll_top_duration = 500,
    element = document.getElementById('scroll-to-top')

    $(window).scroll(function () {
        $(this).scrollTop() > offset ? $back_to_top.addClass('visible') : $back_to_top.removeClass('visible');
    });

    $back_to_top.on('click', function (e) {
        e.preventDefault();
        $('body,html').animate({
            scrollTop: 0
        }, scroll_top_duration);
    });
