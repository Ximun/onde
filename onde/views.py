from django.views.generic.list import ListView
from .models import Actu, Date, PhotoPresse, Galerie, Video, Contact, Section
from django.shortcuts import get_object_or_404, render

class ActuListView(ListView):
    model = Actu
    template_name = "onde/pages/home.html"

class DateListView(ListView):
    model = Date
    template_name = "onde/pages/agenda.html"

class GalerieListView(ListView):
    model = Galerie
    template_name = "onde/pages/photos.html"

class VideoListView(ListView):
    model = Video
    template_name = "onde/pages/videos.html"

class ContactListView(ListView):
    model = Contact
    template_name = "onde/pages/contact.html"

# Extra functions

def getBios():
    bios = {bio['nom']:bio['contenu'] for bio in Section.objects.values()}
    return bios

def getPresse():
    return PhotoPresse.objects.all()
