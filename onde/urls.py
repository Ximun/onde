from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView
from .views import ActuListView, DateListView, GalerieListView, VideoListView, ContactListView, getBios, getPresse

urlpatterns = [
    path('', ActuListView.as_view(), name="accueil"),
    path('videos', VideoListView.as_view(extra_context={'title':'Vidéos'}), name="videos"),
    path('groupe', TemplateView.as_view(template_name="onde/pages/groupe.html",extra_context={'title':'Le Groupe','bios':getBios}), name="groupe"),
    path('photos', GalerieListView.as_view(extra_context={'title':'Photos','presses':getPresse}), name="photos"),
    path('musique', TemplateView.as_view(template_name="onde/pages/musique.html",extra_context={'title':'Musique'}), name="musique"),
    path('agenda', DateListView.as_view(extra_context={'title':'Agenda'}), name="agenda"),
    path('contact', ContactListView.as_view(extra_context={'title':'Contact'}), name="contact"),
    path('mentions-legales', TemplateView.as_view(template_name="onde/pages/mentions-legales.html",extra_context={'title':'Mentions Légales'}), name="mentions-legales"),
    path('admin/', admin.site.urls),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
