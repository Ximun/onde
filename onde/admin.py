from django.contrib import admin
from .models import Actu, Date, Ville, PhotoPresse, Galerie, GalerieImage, Video, Contact, ContactChamp, ContactCategorie, Section

class ActuAdmin(admin.ModelAdmin):
    list_display = ('titre', 'date', 'publier')
    ordering = ('date',)
    search_fields = ('titre',)

class DateAdmin(admin.ModelAdmin):
    list_display = ('date', 'ville')
    ordering = ('date',)

class PhotoPresseAdmin(admin.ModelAdmin):
    list_display = ('position',)
    ordering = ('position',)

class GalerieImageInline(admin.TabularInline):
    model = GalerieImage

class GalerieAdmin(admin.ModelAdmin):
    list_display = ('nom',)
    inlines = [GalerieImageInline]

class VideoAdmin(admin.ModelAdmin):
    list_display = ('nom',)

class ContactChampInline(admin.TabularInline):
    model = ContactChamp

class ContactAdmin(admin.ModelAdmin):
    list_display = ('nom', 'categorie')
    inlines = [ContactChampInline]

class SectionAdmin(admin.ModelAdmin):
    list_display = ('nom',)


admin.site.register(Section, SectionAdmin)
admin.site.register(Actu, ActuAdmin)
admin.site.register(Date, DateAdmin)
admin.site.register(Ville)
admin.site.register(PhotoPresse, PhotoPresseAdmin)
admin.site.register(Galerie, GalerieAdmin)
admin.site.register(Video, VideoAdmin)
admin.site.register(Contact, ContactAdmin)
admin.site.register(ContactCategorie)
