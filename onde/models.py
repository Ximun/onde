import os
import pdb
from django.db import models
from django.utils import timezone
from django.core.files.base import ContentFile
from django.dispatch import receiver
from ckeditor.fields import RichTextField
from stdimage.models import StdImageField
from os.path import splitext
from io import BytesIO
from PIL import Image
from .functions import resize_and_crop, get_upload_to, presse_thumb

class Actu(models.Model):
    titre = models.CharField(max_length=100, null=False, unique=True)
    contenu = RichTextField()
    date = models.DateTimeField(default=timezone.now)
    first = models.BooleanField(default=False, help_text="Est-ce que l'actu doit être mise en avant?")
    publier = models.BooleanField(help_text="Afficher l'actu?")

    def __str__(self):
        return self.titre

class Date(models.Model):
    date = models.DateTimeField()
    ville = models.ForeignKey('Ville', on_delete=models.CASCADE)
    lieu = models.CharField(max_length=100, null=True)
    facebook = models.URLField(null=True, blank=True, help_text="Lien vers évènement Facebook")
    website = models.URLField(null=True, blank=True, help_text="Lien vers une page web")

    def __str__(self):
        return self.date.strftime('%d %b %Y')

class Ville(models.Model):
    nom = models.CharField(max_length=100, unique=True, null=False)

    def __str__(self):
        return self.nom

class Galerie(models.Model):
    nom = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.nom

class GalerieImage(models.Model):
    galerie = models.ForeignKey(Galerie, related_name="images", on_delete=models.CASCADE)
    image = models.ImageField(upload_to=get_upload_to)
    thumbnail = models.ImageField(upload_to=get_upload_to, editable=False)

    def save(self, *args, **kwargs):
        """
        Redimensionner l'image et créer une vignette
        """
        if not self.size_and_thumbnail():
            raise Exception('Opération sur les images diaporama non valide')
        super().save(*args, **kwargs) # Sauvegarde la photo (pas top mais obligé à envoyer pour traiter non?)

    def size_and_thumbnail(self):
        """
        Réduire l'image postée et créer la vignette
        """
        image = Image.open(self.image)

        thumb_name, thumb_extension = splitext(self.image.name)
        thumb_extension = thumb_extension.lower()
        thumb_filename = thumb_name + '_thumb' + thumb_extension

        if thumb_extension in ['.jpg', '.jpeg']:
            FTYPE = 'JPEG'
        elif thumb_extension == '.gif':
            FTYPE = 'GIF'
        elif thumb_extension == '.png':
            FTYPE = 'PNG'
        else:
            return False # Conserve le format de l'image originale s'il est reconnu

        image_buffer = BytesIO()
        thumb_buffer = BytesIO()
        image.thumbnail((1620,1620), Image.ANTIALIAS)
        image.save(image_buffer, FTYPE)
        image_buffer.seek(0)
        self.image.save(self.image.name, ContentFile(image_buffer.read()), save=False)
        image_buffer.close()

        thumb = resize_and_crop(self.image.path, thumb_filename, (160,160), 'middle')
        thumb.save(thumb_buffer, FTYPE)
        thumb_buffer.seek(0)

        self.thumbnail.save(thumb_filename, ContentFile(thumb_buffer.read()), save=False)
        thumb_buffer.close()

        return True

@receiver(models.signals.post_delete, sender=GalerieImage)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    Supprime l'image et sa vignette correspondante quand un modèle GalerieImage est supprimé
    """
    if instance.image:
        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)
    if instance.thumbnail:
        if os.path.isfile(instance.thumbnail.path):
            os.remove(instance.thumbnail.path)

@receiver(models.signals.pre_save, sender=GalerieImage)
def auto_delete_file_on_change(sender, instance, **kwargs):
    """
    Supprime les anciens fichiers quand un nouveau est posté à la place.
    """
    if not instance.pk:
        return False

    try:
        old_file = GalerieImage.objects.get(pk=instance.pk).image
        old_thumb = GalerieImage.objects.get(pk=instance.pk).thumbnail
    except GalerieImage.DoesNotExist:
        return False

    new_file = instance.image
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)
            os.remove(old_thumb.path)

class Video(models.Model):
    nom = models.CharField(max_length=60, unique=True, blank=False)
    youtubeID = models.CharField(max_length=11, unique=True, blank=False)
    vignette = StdImageField(upload_to="videos/", variations={'thumbnail': (1000, 470, True)}, null=True, delete_orphans=True)
    first = models.BooleanField(default=False, help_text="Est-ce que la vidéo doit être mise en avant? (grand format)")

    def __str__(self):
        return self.nom

@receiver(models.signals.post_save, sender=Video)
def delete_original_image(sender, instance, **kwargs):
    if instance.vignette and os.path.isfile(instance.vignette.path):
        os.remove(instance.vignette.path)

class PhotoPresse(models.Model):
    position = models.IntegerField()
    photo = StdImageField(upload_to="presse/", variations={'thumbnail': (400, 400, True)}, null=False, delete_orphans=True, render_variations=presse_thumb)

    class Meta:
        verbose_name = 'Photo de Presse'
        verbose_name_plural = 'Photos de Presse'

    def __str__(self):
        return str(self.position)

class Contact(models.Model):
    nom = models.CharField(max_length=100, unique=True, blank=False)
    categorie = models.ForeignKey('ContactCategorie', on_delete=models.CASCADE)
    description = RichTextField(unique=False, blank=True)

    def __str__(self):
        return self.nom

class ContactCategorie(models.Model):
    nom = models.CharField(max_length=100, unique=True, null=False)

    def __str__(self):
        return self.nom

class ContactChamp(models.Model):
    contact = models.ForeignKey(Contact, related_name="champs", on_delete=models.CASCADE)
    logo = models.CharField(max_length=20, unique=False, blank=True)
    contenu = models.CharField(max_length=100, unique=False, blank=False)
    lien = models.URLField(max_length=100, unique=False, blank=True)

    def __str__(self):
        return self.logo

class Section(models.Model):
    nom = models.CharField(max_length=100, unique=True, blank=False)
    contenu = RichTextField()

    def __str__(self):
        return self.nom
