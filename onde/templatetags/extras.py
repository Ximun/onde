from django import template
from datetime import date
register = template.Library()

@register.filter
def future(the_date):
    if the_date.date.date() >= date.today():
        return the_date
    else:
        return None

@register.filter
def past(the_date):
    if the_date.date.date() < date.today():
        return the_date
    else:
        return None
